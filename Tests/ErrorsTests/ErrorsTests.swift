import XCTest
@testable import Errors

final class ErrorsTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Errors().text, "Hello, World!")
    }
}
