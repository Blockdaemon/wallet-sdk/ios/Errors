// Copyright © Blockdaemon All rights reserved.

import Foundation

/// Errors returned when there is an unexpected response
public enum HTTPRequestServerError: Error {
    case badResponse
    case emptyHeader
}
